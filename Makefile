# When we use make the fisrt target is automatically called
# <target-name>: dependencies
# |TAB| instuctions to execute

#-I includes directory for header files [compiler flag]
#-L include the library for linking [linker flag]

# use dpkg -S <library_name: exp glib> to get the proper name --> glib-2.0
# use pkg-config --libs glib-2.0 to get linker flags
# use pkg-config --cflags glib-2.0 to get compiler flags
# for mpicc: mpicc --showme:link (linker flags) mpicc --showme:compile (compiler flags)


# This target is ran on defautl: part (b) of the exercise
default: main

# subrouting called by default 
main: fishing.o lotkavolterra.o plotgrid.o fluidboids.o
	mpicc fishing.o lotkavolterra.o plotgrid.o pbPlots.o supportLib.o fluidboids.o -lm -o main

fishing.o: fishing.c
	mpicc -c fishing.c

lotkavoltera.o: lotkavoltera.c 
	gcc -c lotkavoltera.c

plotgrid.o: plotgrid.c plotting/pbPlots.c plotting/supportLib.c
	gcc -c plotgrid.c plotting/pbPlots.c plotting/supportLib.c

# This target runs the part (a) of the exercise
grid: cart2d.o perlin.o lotkavolterra.o
	mpicc cart2d.o perlin.o lotkavolterra.o -lm -o grid
	mpirun -np 16 --oversubscribe ./grid

# subroutine called by grid
perlin.o: perlin.c perlin.h
	mpicc -c perlin.c

cart2d.o: cart2d.c
	mpicc -c cart2d.c

fluidboids.o: fluidboids.c fluidboids.h
	gcc -c fluidboids.c

# run only the test for perlin
perlin_test: perlin.o perlin_test.c
	gcc perlin_test.c perlin.o -lm  -o perlin
	./perlin

# run only the test for perlin
lotka_test: lotkavolterra.c lotka_test.c plotting/pbPlots.c plotting/supportLib.c
	gcc lotka_test.c lotkavolterra.c plotting/pbPlots.c plotting/supportLib.c -o lotka -lm
	./lotka

# not called by default. To call it use make run command
run:
	mpirun -np 36 --oversubscribe ./main

# remove all binaries and object files
clean:
	rm -f *.o main perlin lotka grid fishing *.log all_logs *.txt log
