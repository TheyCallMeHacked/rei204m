#include "lotkavolterra.h"

tuple evaluate(const Matrix2d coefficients, const tuple x)
{
    float x0 = x.first;
    float x1 = x.second;

    float y0 = coefficients[0][0] * x0 + coefficients[0][1] * x0 * x1;
    float y1 = coefficients[1][0] * x0 * x1 + coefficients[1][1] * x1;

    tuple y = {y0, y1};

    return y;
}

tuple lotka_volterra(const tuple init, const float alpha, const float beta, const float gamma, const float delta, const int time, const int n_steps)
{

    // calculate number of steps for the discrete time
    float step_size = (float)time / n_steps;

    const Matrix2d coefficients = {
        {alpha, -beta},
        {delta, -gamma}};

    // solve discretized ODE using implicit midpoint
    tuple current = init;

    for (int i = 0; i < n_steps; i++)
    {
        // perform a step of implicit midpoint
        current = explicit_midpoint(current, coefficients, step_size);
    }

    return current;
}

tuple explicit_midpoint(const tuple init, const Matrix2d coefficients, const float h)
{
    float x0 = init.first;
    float x1 = init.second;

    // calculate the midpoint derivative
    tuple k1 = evaluate(coefficients, init);
    tuple next = {init.first + h * k1.first / 2, init.second + h * k1.second / 2};

    tuple k2 = evaluate(coefficients, next);

    // use midpoint derivative to evolve the problem
    float y0 = x0 + h * k2.first;
    float y1 = x1 + h * k2.second;

    tuple y = {y0, y1};

    // return the next step
    return y;
}

tuple matrix_vector_mult(const Matrix2d A, const tuple x)
{
    float x0 = x.first;
    float x1 = x.second;

    float y0 = A[0][0] * x0 + A[0][1] * x1;
    float y1 = A[1][0] * x0 + A[1][1] * x1;

    tuple y = {y0, y1};

    return y;
}
