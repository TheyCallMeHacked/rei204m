#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>
#include "perlin.h"

#define ROW 6
#define COL 6
#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3

#define THRESHOLD 0.1f //Perlin Noise produces value from 0 to 1
#define LANDMASS 0
#define WATER 1


int main (int argc, char** argv) {
	int numtasks, rank, source, dest, i, tag=1;
	int inbuf[4] = {MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL};
	int nbrs[4];

	//Indicate dimensions
	int dims[2] = {ROW, COL};
	numtasks = ROW * COL;

	//Period param as logical array specifying whether cartesian grid is period
	//period = negative shift/turnaround, non periodic = off-end shift -> MPI_PROC_NULL
	int periods[2] = {1,1};

	int coords[2];
	int reorder = 1;

	MPI_Comm comm_2d;
	MPI_Request reqs[8];
	MPI_Status stats[8];

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	// Create new communicators (cartesian structure)
	MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &comm_2d);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	//Obtains process coordinates in cartesian topology and cartesian rank
	int cart_rank;
	MPI_Cart_coords(comm_2d, rank, 2, coords);
	MPI_Cart_rank(comm_2d, coords, &cart_rank);

	// Determine if the grid cell is part of the landmass or water
	int cell_type = perlin(coords[0]*0.1f, coords[1]*0.1f) > THRESHOLD;
	int nbrs_type[4] = {MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL}; //(udlr)


	//Obtain ranks for shifting data in cartesian topology
	//MPI_Cart_shift(MPI_Comm comm, int dir, int displ, int *source, int *dest)
	//To right - displ = +1, to left - displ = -1
	MPI_Cart_shift(comm_2d, 0, 1, &nbrs[UP], &nbrs[DOWN]);
	MPI_Cart_shift(comm_2d, 1, 1, &nbrs[LEFT], &nbrs[RIGHT]);

	printf("rank=%d, cart_rank=%d, coords=(%d,%d), celltype=(%d), neighbours(udlr)=(%d,%d,%d,%d)\n", rank, cart_rank, coords[0], coords[1], cell_type, nbrs[UP], nbrs[DOWN], nbrs[LEFT], nbrs[RIGHT]);

	//Store grid cell type of neigbours

	for (i=0; i<4; i++) {
			dest=nbrs[i];
			source=nbrs[i];
			//NON BLOCKING
			MPI_Isend(&cell_type, 1, MPI_INT, dest, tag, MPI_COMM_WORLD, &reqs[i]);
			MPI_Irecv(&nbrs_type[i], 1, MPI_INT, source, tag, MPI_COMM_WORLD, &reqs[i+4]);
	}

	//Wait for all non-blocking communication to be completed
	MPI_Waitall(8, reqs, stats);

	printf("rank=%d, cart_rank=%d, celltype_of_neighbours(udlr)=(%d,%d,%d,%d)\n", rank, cart_rank, nbrs_type[UP], nbrs_type[DOWN], nbrs_type[LEFT], nbrs_type[RIGHT]);

	//DO SOME WORK with MPI communicators

	MPI_Barrier(MPI_COMM_WORLD);

	//Print Grid
	int gathered_cell_types[numtasks];  // Array to store gathered cell_type values

	//Gather cell_type values from all processes onto root (rank 0)
	MPI_Gather(&cell_type, 1, MPI_INT, gathered_cell_types, 1, MPI_INT, 0, MPI_COMM_WORLD);

	if (rank == 0) {
			int grid[ROW * COL];
			for (int i = 0; i < ROW * COL; i++) {
					int c[2];
					MPI_Cart_coords(comm_2d, i, 2, c);
					grid[(c[0] * COL + c[1])] = gathered_cell_types[i];
			}
			for (int i = 0; i < ROW; i++) {
					for (int j = 0; j < COL; j++) {
							printf("%d ", grid[i * COL + j]);
					}
					printf("\n");
			}
			for (int i = 0; i < ROW; i++) {
					for (int j = 0; j < COL; j++) {
							if (grid[i * COL + j]) {
									printf("X");
							} else {
									printf(".");
							}
					}
					printf("\n");
			}
	}
	MPI_Finalize();
	return 0;
}
