#include "fluidboids.h"
#include <math.h>
#include <stdio.h>

#define MAX_ACC 1e100
#define MAX_VEL 1.

double dot(Vector v1, Vector v2) {
    return v1.x*v2.x + v1.y*v2.y;
}

double len(Vector v) {
    return sqrt(dot(v,v));
}

void separate(unsigned int self_count, Vector* self_vel, unsigned int nbrh_count[4], Vector nbrh_vel[4], Vector* acc);
void align(unsigned int self_count, Vector* self_vel, unsigned int nbrh_count[4], Vector nbrh_vel[4], Vector* acc);
void cohere(unsigned int self_count, Vector* self_vel, unsigned int nbrh_count[4], Vector nbrh_vel[4], Vector* acc);

void fluidboid_update(
        unsigned int self_count,
        Vector* self_vel,
        unsigned int nbrh_count[4],
        Vector nbrh_vel[4],
        double deltatime,
        Vector* new_vel
) {
    Vector acc = { 0., 0. };

    if(self_count + nbrh_count[UP] + nbrh_count[DOWN] + nbrh_count[LEFT] + nbrh_count[RIGHT]) {
        separate(self_count, self_vel, nbrh_count, nbrh_vel, &acc);
        align(self_count, self_vel, nbrh_count, nbrh_vel, &acc);
        cohere(self_count, self_vel, nbrh_count, nbrh_vel, &acc);
    }
    
    new_vel->x += acc.x * deltatime;
    new_vel->y += acc.y * deltatime;

    double v = len(*new_vel);
    if(v > MAX_VEL) {
        new_vel->x /= v/MAX_VEL;
        new_vel->y /= v/MAX_VEL;
    }
}

void separate(unsigned int self_count, Vector* self_vel, unsigned int nbrh_count[4], Vector nbrh_vel[4], Vector* acc) {
    unsigned int max = 0;
    unsigned int min = 0;
    for (size_t i = 0; i < 4; i++) {
        if (nbrh_count[i] < nbrh_count[min]) {
            min = i;
        }
        else if (nbrh_count[i] > nbrh_count[max]) {
            max = i;
        }
    }

    if (nbrh_count[max] < self_count) {
        acc->x += DIRECTION(min).x;
        acc->y += DIRECTION(min).y;
    }
    else {
        acc->x -= DIRECTION(max).x;
        acc->y -= DIRECTION(max).y;
    }
}

void align(unsigned int self_count, Vector* self_vel, unsigned int nbrh_count[4], Vector nbrh_vel[4], Vector* acc) {
    Vector average = { 0., 0. };
    for (size_t i = 0; i < 4; i++) {
        average.x += nbrh_vel[i].x*nbrh_count[i];
        average.y += nbrh_vel[i].y*nbrh_count[i];
    }

    average.x += self_vel->x;
    average.y += self_vel->y;

    unsigned int sum = self_count + nbrh_count[UP] + nbrh_count[DOWN] + nbrh_count[LEFT] + nbrh_count[RIGHT];

    average.x /= sum;
    average.y /= sum;

    acc->x += average.x - self_vel->x;
    acc->y += average.y - self_vel->x;
}

void cohere(unsigned int self_count, Vector* self_vel, unsigned int nbrh_count[4], Vector nbrh_vel[4], Vector* acc) {
    Vector average = { 0., 0. };
    for (size_t i = 0; i < 4; i++) {
        average.x += DIRECTION(i).x*nbrh_count[i];
        average.y += DIRECTION(i).y*nbrh_count[i];
    }

    unsigned int sum = self_count + nbrh_count[UP] + nbrh_count[DOWN] + nbrh_count[LEFT] + nbrh_count[RIGHT];

    average.x /= sum;
    average.y /= sum;

    acc->x += average.x;
    acc->y += average.y;
}

