#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "plotting/pbPlots.h"
#include "plotting/supportLib.h"
#include "plotgrid.h"

void plot_grid(unsigned int *fish, unsigned int n_fish, unsigned int *sharks, unsigned int n_sharks, unsigned int rows, unsigned int cols, unsigned int time, tuple boat)
{
    double x_fish[n_fish + 1], y_fish[n_fish + 1];
    double x_sharks[n_sharks + 1], y_sharks[n_sharks + 1];
    unsigned int fish_pointer = 0U;
    unsigned int shark_pointer = 0U;

    // put fish and shark with random offset inside the grid cells
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            // plot fish by placing every of them randomly inside the grid square
            unsigned int n_fish_cell = fish[i * cols + j];
            for (unsigned int k = 0; k < n_fish_cell; k++)
            {
                x_fish[fish_pointer] = i + ((float)rand() / (RAND_MAX));
                y_fish[fish_pointer] = j + ((float)rand() / (RAND_MAX));

                fish_pointer++;
            }

            // plot sharks by placing every of them randomly inside the grid square
            unsigned int n_shark_cell = sharks[i * cols + j];
            for (unsigned int k = 0; k < n_shark_cell; k++)
            {
                x_sharks[shark_pointer] = i + ((float)rand() / (RAND_MAX));
                y_sharks[shark_pointer] = j + ((float)rand() / (RAND_MAX));

                shark_pointer++;
            }
        }
    }

    assert(shark_pointer == n_sharks && "Wrong number of sharks provided!");
    assert(fish_pointer == n_fish && "Wrong number of fish provided!");

    // cover special cases: sharks extinct
    if (n_sharks == 0)
    {
        n_sharks = 1;
        x_sharks[0] = y_sharks[0] = 0.0;
    }

    // cover special cases: fish extinct
    if (n_fish == 0)
    {
        n_fish = 1;
        x_fish[0] = y_fish[0] = 0.0;
    }

    StartArenaAllocator();

    // plot the grid usign blue triangles for fish and red crosses for sharks
    RGBABitmapImageReference *canvasReference = CreateRGBABitmapImageReference();

    // prey plot
    ScatterPlotSeries *series = GetDefaultScatterPlotSeriesSettings();
    series->xs = x_fish;
    series->xsLength = n_fish;
    series->ys = y_fish;
    series->ysLength = n_fish;
    series->linearInterpolation = false;
    series->pointType = L"triangles";
    series->pointTypeLength = wcslen(series->pointType);
    series->color = CreateRGBColor(0, 0, 1);

    // predator plot
    ScatterPlotSeries *series2 = GetDefaultScatterPlotSeriesSettings();
    series2->xs = x_sharks;
    series2->xsLength = n_sharks;
    series2->ys = y_sharks;
    series2->ysLength = n_sharks;
    series2->linearInterpolation = false;
    series2->pointType = L"dots";
    series2->pointTypeLength = wcslen(series2->pointType);
    series2->color = CreateRGBColor(1, 0, 0);

    ScatterPlotSettings *settings = GetDefaultScatterPlotSettings();
    settings->width = 600;
    settings->height = 600;
    settings->autoBoundaries = false;
    settings->xMax = cols;
    settings->xMin = 0;
    settings->yMax = rows;
    settings->yMin = 0;
    settings->autoPadding = true;
    settings->title = L"Fish & Shark population";
    settings->titleLength = wcslen(settings->title);
    settings->xLabel = L"X coord";
    settings->xLabelLength = wcslen(settings->xLabel);
    settings->yLabel = L"Y coord";
    settings->yLabelLength = wcslen(settings->yLabel);

    // combine two plots in one
    ScatterPlotSeries *s[] = {series, series2};
    settings->scatterPlotSeries = s;
    settings->scatterPlotSeriesLength = 2;

    StringReference *errorMessage = CreateStringReference(L"", 0);
    DrawScatterPlotFromSettings(canvasReference, settings, errorMessage);

    // draw a line
    double x1 = MapXCoordinateBasedOnSettings(boat.first, settings);
    double y1 = MapYCoordinateBasedOnSettings(boat.second, settings);

    double x2 = MapXCoordinateBasedOnSettings(boat.first + 1, settings);
    double y2 = MapYCoordinateBasedOnSettings(boat.second + 1, settings);

    DrawFilledCircle(canvasReference->image, (x2 + x1) / 2.0, (y1 + y2) / 2.0, (x2 - x1) / 4.0, CreateRGBColor(0.0, 1.0, 0.0));

    char filename[100];
    sprintf(filename, "simulation/grid-snapshot%d.png", time);

    ByteArray *pngdata = ConvertToPNG(canvasReference->image);
    WriteToFile(pngdata, filename);
    DeleteImage(canvasReference->image);

    FreeAllocations();
}