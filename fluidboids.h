#ifndef __BOIDS_H__
#define __BOIDS_H__

#include <stddef.h>

#define DOWN 0
#define RIGHT 1
#define LEFT 2
#define UP 3

typedef struct {
    double x;
    double y;
} Vector;

void fluidboid_update(unsigned int self_count, Vector* self_vel, unsigned int nbrh_count[4], Vector nbrh_vel[4], double deltatime, Vector* new_vel);

double dot(Vector v1, Vector v2);

#define DIRECTION(d) (((Vector[]){[UP] = {0, 1}, [DOWN] = {0, -1}, [LEFT] = {-1, 0}, [RIGHT] = {1, 0}})[(d)])

/*
const Vector direction[4] = {
    [UP]    = { 0,  1},
    [DOWN]  = { 0, -1}, 
    [LEFT]  = {-1,  0}, 
    [RIGHT] = { 1,  0}
};
*/

#endif // __BOIDS_H__
