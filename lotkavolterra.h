#ifndef _LOTKA_VOLTERRA_
#define _LOTKA_VOLTERRA_

typedef struct
{
    float first;
    float second;
} tuple;

typedef float Matrix2d[2][2];

/**
 * @brief Evaluate the Lotka-Volterra function LV at point x
 *
 * @param coefficients the evolution parameters packed in the matrix
 * @param x evaluation point
 * @return LV(x)
 */
tuple evaluate(const Matrix2d coefficients, const tuple x);

/**
 * @brief This function solves a discretized Lotka-Volterra ODE using implicit midpoint rule
 * See more: https://en.wikipedia.org/wiki/Lotka-Volterra_equations
 *
 * @param init tuple describing initial conditions for the ODE : [density for prey, density for predators] for t = 0
 * @param alpha
 * @param beta
 * @param gamma
 * @param delta
 * @param time simulation duration, measured in discrete time, where 1 unit corresponds to one iteration on the grod communicator
 * @param n_steps discretization precision, recommended to use at least as large as the time parameter
 * @return tuple containing prey and predator density for the grid
 */
tuple lotka_volterra(const tuple init, const float alpha, const float beta, const float gamma, const float delta, const int time, const int n_steps);

/**
 * @brief Perform one discretization step of LV using explicit midpoint
 * k1 = f(init), k2 = f(init + h/2 * k_1)
 * next = init + h * k2
 *
 * @param init initial condition
 * @param coefficients coefficient amtrix for LV problem
 * @param h step size
 * @return tuple
 */
tuple explicit_midpoint(const tuple init, const Matrix2d coefficients, const float h);

/**
 * @brief Multiply a 2d matrix with a vector
 *
 * @param A 2x2 matrix
 * @param x 2d vector
 * @return y = Ax
 */
tuple matrix_vector_mult(const Matrix2d A, const tuple x);

#endif