#include <stdio.h>
#include <stdlib.h>
#include "lotkavolterra.h"
#include "plotting/pbPlots.h"
#include "plotting/supportLib.h"

// checking whether the numerical implementation is correct
// use example form wikipedia
int main(int argc, char **argv)
{
    // 10 fish and sharks
    tuple init = {6.0, 3.0};
    int time = 100;

    double prey[time + 1];
    double predator[time + 1];
    double time_axis[time + 1];

    float alpha = 0.1;
    float beta = 0.05;
    float delta = 0.05;
    float gamma = 0.3;

    int n_steps_per_unit = 30;

    // initialize initial conditions
    tuple current = init;
    prey[0] = (double)init.first;
    predator[0] = (double)init.second;
    time_axis[0] = 0.0;

    for (int i = 1; i <= time; i++)
    {
        current = lotka_volterra(current, alpha, beta, gamma, delta, 1, n_steps_per_unit);

        // fill the time evolution values
        prey[i] = (double)current.first;
        predator[i] = (double)current.second;
        time_axis[i] = time_axis[i - 1] + 1.0;
    }

    StartArenaAllocator();

    // plot the time evolution
    RGBABitmapImageReference *canvasReference = CreateRGBABitmapImageReference();

    // prey plot
    ScatterPlotSeries *series = GetDefaultScatterPlotSeriesSettings();
    series->xs = time_axis;
    series->xsLength = time + 1;
    series->ys = prey;
    series->ysLength = time + 1;
    series->color = CreateRGBColor(0, 0, 1);

    // predator plot
    ScatterPlotSeries *series2 = GetDefaultScatterPlotSeriesSettings();
    series2->xs = time_axis;
    series2->xsLength = time + 1;
    series2->ys = predator;
    series2->ysLength = time + 1;
    series2->color = CreateRGBColor(1, 0, 0);

    ScatterPlotSettings *settings = GetDefaultScatterPlotSettings();
    settings->width = 600;
    settings->height = 400;
    settings->autoBoundaries = false;
    settings->xMax = time;
    settings->xMin = 0;
    settings->yMax = 10.0;
    settings->yMin = 0;
    settings->autoPadding = true;
    settings->title = L"Predator-Prey model";
    settings->titleLength = wcslen(settings->title);
    settings->xLabel = L"Time";
    settings->xLabelLength = wcslen(settings->xLabel);
    settings->yLabel = L"Density";
    settings->yLabelLength = wcslen(settings->yLabel);

    // combine two plots in one
    ScatterPlotSeries *s[] = {series, series2};
    settings->scatterPlotSeries = s;
    settings->scatterPlotSeriesLength = 2;

    StringReference *errorMessage = CreateStringReference(L"", 0);
    DrawScatterPlotFromSettings(canvasReference, settings, errorMessage);

    ByteArray *pngdata = ConvertToPNG(canvasReference->image);
    WriteToFile(pngdata, "prey-predator.png");
    DeleteImage(canvasReference->image);

    FreeAllocations();

    return 0;
}