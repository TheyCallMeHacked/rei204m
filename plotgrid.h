#ifndef _PLOT_GRID_
#define _PLOT_GRID_

#include "lotkavolterra.h"

void plot_grid(unsigned int *fish, unsigned int n_fish, unsigned int *sharks, unsigned int n_sharks, unsigned int rows, unsigned int cols, unsigned int time, tuple boat);

#endif