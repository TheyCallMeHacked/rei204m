#!/bin/bash
#SBATCH -J cart-test
#SBATCH --partition=Jotunn
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
module load gnu12/12.3.0
module load mpi/openmpi4/4.1.2
make
make run
