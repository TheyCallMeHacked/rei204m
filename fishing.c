#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>
#include "lotkavolterra.h"
#include "plotgrid.h"
#include "fluidboids.h"

#define ROW 6
#define COL 6

#define TIME 100
#define DELTATIME 0.01

#define BINOMIAL 0
#define CAPACITY 50
#define CATCH 10

#define SCALE 9
#define POPULATION_FISH 0.66
#define POPULATION_SHARK 0.33

#define OFFSET 5

#define LANDMASS 0
#define WATER 1

#define FILENAME "log.txt"

/**
 * @brief Generate a Bernoulli random variable ~Be(p)
 *
 * @param p parameter
 * @return sample value from {0, 1}
 */
unsigned int bernoulli(float p)
{
    float uniform = ((float)rand() / (RAND_MAX));
    int br = 0;
    if (uniform <= p)
    {
        br = 1;
    }
    return br;
}

static inline double positive(double x) {
    return x > 0 ? x : 0;
}

static inline int clamp(int x, int min, int max) {
    return x >= min ? (x <= max ? x : 0) : 0;
}

int main(int argc, char **argv)
{
    int numtasks, rank, source, dest, i, tag = 1;
    int inbuf[4] = {MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL};

    // Indicate dimensions
    int dims[2] = {ROW, COL};
    numtasks = ROW * COL;

    // initialize the global MPI Suite and get global rank info
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Create MPI datatype to represent Vectors
    MPI_Datatype MPI_VECTOR;
    MPI_Type_vector(1, 2, 2, MPI_DOUBLE, &MPI_VECTOR);
    MPI_Type_commit(&MPI_VECTOR);

    // Create new communicator (cartesian structure)
    // period param as logical array specifying whether cartesian grid is period
    // period = negative shift/turnaround, non periodic = off-end shift -> MPI_PROC_NULL
    int periods[2] = {1, 1};
    int reorder = 0;
    MPI_Comm comm_2d;
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &comm_2d);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Obtain process coordinates in cartesian topology and cartesian rank
    int rank_2d;
    int coords[2];
    MPI_Cart_coords(comm_2d, rank, 2, coords);
    MPI_Cart_rank(comm_2d, coords, &rank_2d);

    // initialize random number (use rank to ensure that they are different)
    time_t t;
    srand((unsigned)time(&t) * rank);

    // initialize parameters for sharks and fish
    float alpha = 0.1;
    float beta = 0.05;
    float delta = 0.05;
    float gamma = 0.3;

    // initialize the content of each cell
    // initial population of fish and shark is set approx POPULATION_INIT (except for harbour)
    int cell = (rank == 0) ? LANDMASS : WATER;
    int boat = (rank == 0) ? true : false;
    int boat_value = 0;

    unsigned int fish = 0;
    unsigned int shark = 0;
    if (rank != 0)
    {
        if (BINOMIAL)
        {
            for (int i = 0; i < SCALE; i++)
            {
                fish += bernoulli(POPULATION_FISH);
                shark += bernoulli(POPULATION_SHARK);
            }
        }
        else
        {
            fish = SCALE * bernoulli(POPULATION_FISH);
            shark = SCALE * bernoulli(POPULATION_SHARK);
        }
    }
    Vector fish_vel = {0., 0.};
    Vector shark_vel = {0., 0.};

    // array containing neighbours coordinates [comm2d]
    int nbrs_2d[4];

    // initialize arrays containing information about the neighbours [comm2d]
    int nbrs_type[4], nbrs_boat_value[4], nbrs_boat_receiver[4];
    unsigned int nbrs_fish[4], nbrs_shark[4];
    Vector nbrs_fish_vel[4], nbrs_shark_vel[4];
    int nbrs_boat_indicator[4] = {0, 0, 0, 0};

    // Receive rank numbers for neighbours to {UP, DOWN, LEFT, RIGHT}
    // To right - displ = +1, to left - displ = -1
    MPI_Cart_shift(comm_2d, 1, 1, &nbrs_2d[DOWN], &nbrs_2d[UP]);
    MPI_Cart_shift(comm_2d, 0, 1, &nbrs_2d[LEFT], &nbrs_2d[RIGHT]);

    // Create a filename based on the process rank
    MPI_File outFile;
    MPI_Status status;
    MPI_File_open(MPI_COMM_WORLD, FILENAME, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &outFile);

    //***************************************************
    //****************REPEAT K TIMES*********************
    //***************************************************

    // perform the time-evolution for K timesteps
    for (unsigned t = 0; t <= TIME; t++)
    {
        // --------------------------------------------------
        // ----------------NON BLOCKING BEGIN----------------
        // --------------------------------------------------
        MPI_Request reqs[56];
        MPI_Status stats[56];

        if (rank == 0)
        {
            printf("Timestep %d \n", t);
        }

        // Step I: gather information about the landmass around you
        for (i = 0; i < 4; i++)
        {
            dest = nbrs_2d[i];
            source = nbrs_2d[i];

            // send your rank information to all neighbours
            MPI_Isend(&cell, 1, MPI_INT, dest, tag, comm_2d, &reqs[i]);

            // receive your rank information from all neighbours and store it in nbrs_type
            MPI_Irecv(&nbrs_type[i], 1, MPI_INT, source, tag, comm_2d, &reqs[i + 4]);
        }

        // Step II: gather information abouth fish around you
        for (i = 0; i < 4; i++)
        {
            dest = nbrs_2d[i];
            source = nbrs_2d[i];

            // send your rank information to all neighbours
            MPI_Isend(&fish, 1, MPI_UNSIGNED, dest, tag, comm_2d, &reqs[i + 8]);

            // receive your rank information from all neighbours and store it in nbrs_type
            MPI_Irecv(&nbrs_fish[i], 1, MPI_UNSIGNED, source, tag, comm_2d, &reqs[i + 12]);

            // send your fish's average velocity to all neighbours
            MPI_Isend(&fish_vel, 1, MPI_VECTOR, dest, tag, comm_2d, &reqs[i + 16]);

            // receive your neighbours' fish's average velocity
            MPI_Irecv(&nbrs_fish_vel[i], 1, MPI_VECTOR, source, tag, comm_2d, &reqs[i + 20]);
        }

        // Step III: gather information about sharks around you
        for (i = 0; i < 4; i++)
        {
            dest = nbrs_2d[i];
            source = nbrs_2d[i];

            // send your rank information to all neighbours
            MPI_Isend(&shark, 1, MPI_UNSIGNED, dest, tag, comm_2d, &reqs[i + 24]);

            // receive your rank information from all neighbours and store it in nbrs_type
            MPI_Irecv(&nbrs_shark[i], 1, MPI_UNSIGNED, source, tag, comm_2d, &reqs[i + 28]);

            // send your sharks' average velocity to all neighbours
            MPI_Isend(&shark_vel, 1, MPI_VECTOR, dest, tag, comm_2d, &reqs[i + 32]);

            // receive your neighbours' sharks' average velocity
            MPI_Irecv(&nbrs_shark_vel[i], 1, MPI_VECTOR, source, tag, comm_2d, &reqs[i + 36]);
        }


        // Step IV: send boat_indicator
        for (i = 0; i < 4; i++)
        {
            dest = nbrs_2d[i];
            source = nbrs_2d[i];

            // if boat_indicator is sending to specified direction
            MPI_Isend(&nbrs_boat_indicator[i], 1, MPI_INT, dest, tag, comm_2d, &reqs[i + 40]);

            // receive your rank information from all neighbours and store it in nbrs_type
            MPI_Irecv(&nbrs_boat_receiver[i], 1, MPI_INT, source, tag, comm_2d, &reqs[i + 44]);
        }

        // Step V: send boat_indicator value
        for (i = 0; i < 4; i++)
        {
            dest = nbrs_2d[i];
            source = nbrs_2d[i];

            // if boat_indicator is sending to specified direction
            MPI_Isend(&boat_value, 1, MPI_INT, dest, tag, comm_2d, &reqs[i + 48]);

            // receive your rank information from all neighbours and store it in nbrs_type
            MPI_Irecv(&nbrs_boat_value[i], 1, MPI_INT, source, tag, comm_2d, &reqs[i + 52]);
        }
        
        MPI_Waitall(56, reqs, stats);

        // Fish movement
        MPI_Request reqsfish[8];
        MPI_Status statsfish[8];
        Vector new_fish_vel;
        fluidboid_update(fish, &fish_vel, nbrs_fish, nbrs_fish_vel, DELTATIME, &new_fish_vel);
        int delta_fish = 0;
        double nbrs_delta_fish_dbl[4];
        nbrs_delta_fish_dbl[UP]    = (fish * positive(dot(DIRECTION(UP),    new_fish_vel)) * DELTATIME);
        nbrs_delta_fish_dbl[DOWN]  = (fish * positive(dot(DIRECTION(DOWN),  new_fish_vel)) * DELTATIME);
        nbrs_delta_fish_dbl[LEFT]  = (fish * positive(dot(DIRECTION(LEFT),  new_fish_vel)) * DELTATIME);
        nbrs_delta_fish_dbl[RIGHT] = (fish * positive(dot(DIRECTION(RIGHT), new_fish_vel)) * DELTATIME);
        int max = fish;
        int nbrs_delta_fish[4];
        nbrs_delta_fish[UP]    = clamp((int)trunc(fish * positive(dot(DIRECTION(UP),    new_fish_vel)) * DELTATIME), 0, max);
        max -= nbrs_delta_fish[UP];
        nbrs_delta_fish[DOWN]  = clamp((int)trunc(fish * positive(dot(DIRECTION(DOWN),  new_fish_vel)) * DELTATIME), 0, max);
        max -= nbrs_delta_fish[DOWN];
        nbrs_delta_fish[LEFT]  = clamp((int)trunc(fish * positive(dot(DIRECTION(LEFT),  new_fish_vel)) * DELTATIME), 0, max);
        max -= nbrs_delta_fish[LEFT];
        nbrs_delta_fish[RIGHT] = clamp((int)trunc(fish * positive(dot(DIRECTION(RIGHT), new_fish_vel)) * DELTATIME), 0, max);
        int incoming_delta_fish[4];
        
        // Share fish changes with neighbours
        for (i = 0; i < 4; i++)
        {
            dest = nbrs_2d[i];
            source = nbrs_2d[i];

            MPI_Isend(&nbrs_delta_fish[i], 1, MPI_INT, dest, tag, comm_2d, &reqsfish[i]);

            MPI_Irecv(&incoming_delta_fish[i], 1, MPI_INT, source, tag, comm_2d, &reqsfish[i + 4]);
        }
        MPI_Waitall(8, reqsfish, statsfish);

        // Tally up fish that entered and left the cell
        for (i = 0; i < 4; i++) {
            delta_fish += incoming_delta_fish[i];
            delta_fish -= nbrs_delta_fish[i];
        }
        fish += delta_fish;

        // Shark movement
        MPI_Request reqssharks[8];
        MPI_Status statssharks[8];
        Vector new_shark_vel;
        fluidboid_update(shark, &shark_vel, nbrs_shark, nbrs_shark_vel, DELTATIME, &new_shark_vel);
        int delta_shark = 0;
        max = shark;
        int nbrs_delta_shark[4];
        nbrs_delta_shark[UP]    = clamp((int)trunc(shark * positive(dot(DIRECTION(UP),    new_shark_vel)) * DELTATIME), 0, max);
        max -= nbrs_delta_shark[UP];
        nbrs_delta_shark[DOWN]  = clamp((int)trunc(shark * positive(dot(DIRECTION(DOWN),  new_shark_vel)) * DELTATIME), 0, max);
        max -= nbrs_delta_shark[DOWN];
        nbrs_delta_shark[LEFT]  = clamp((int)trunc(shark * positive(dot(DIRECTION(LEFT),  new_shark_vel)) * DELTATIME), 0, max);
        max -= nbrs_delta_shark[LEFT];
        nbrs_delta_shark[RIGHT] = clamp((int)trunc(shark * positive(dot(DIRECTION(RIGHT), new_shark_vel)) * DELTATIME), 0, max);
        int incoming_delta_shark[4];
        
        // Share shark changes with neighbours
        for (i = 0; i < 4; i++)
        {
            dest = nbrs_2d[i];
            source = nbrs_2d[i];

            MPI_Isend(&nbrs_delta_shark[i], 1, MPI_INT, dest, tag, comm_2d, &reqssharks[i]);

            MPI_Irecv(&incoming_delta_shark[i], 1, MPI_INT, source, tag, comm_2d, &reqssharks[i + 4]);
        }
        MPI_Waitall(8, reqssharks, statssharks);

        // Tally up sharks that entered and left the cell
        for (i = 0; i < 4; i++) {
            delta_shark += incoming_delta_shark[i];
            delta_shark -= nbrs_delta_shark[i];
        }
        shark += delta_shark;
        

        if (t != 0) {
            boat = nbrs_boat_receiver[0] || nbrs_boat_receiver[1] || nbrs_boat_receiver[2] || nbrs_boat_receiver[3];
        }

        if (boat) {
            if (nbrs_boat_receiver[0]) {
                boat_value = nbrs_boat_value[0];
            } else if (nbrs_boat_receiver[1]) {
                boat_value = nbrs_boat_value[1];
            } else if (nbrs_boat_receiver[2]) {
                boat_value = nbrs_boat_value[2];
            } else {
                boat_value = nbrs_boat_value[3];
            }
        }

        //BOAT
        if (boat) {

            // Determine catch number
            int num_to_catch = fish > CATCH ? CATCH : fish; // if there is more fish then max catch count, return max catch count
            int cap_left = CAPACITY - boat_value;
            num_to_catch = num_to_catch > cap_left ? cap_left : num_to_catch; // if there is more to catch than catching capacity, return catching capacity
            
            fish = fish - num_to_catch;
            boat_value = boat_value + num_to_catch;

            if (boat_value == CAPACITY) {
                // Boat is at habour
                if (coords[0] == 0 && coords[1] == 0) {

                    boat_value = 0;

                    // randomly choose new place to go to
                    int dir_1 = bernoulli(0.5);
                    int dir_2 = bernoulli(0.5);

                    if (!dir_1 && !dir_2) {
                        nbrs_boat_indicator[UP] = true;
                    } else if (!dir_1 && dir_2) {
                        nbrs_boat_indicator[RIGHT] = true;
                    } else if (dir_1 && !dir_2) {
                        nbrs_boat_indicator[DOWN] = true;
                    } else {
                        nbrs_boat_indicator[LEFT] = true;
                    }
                } else {

                    // return to habour
                    if (coords[0] > 0) {
                        nbrs_boat_indicator[LEFT] = true;
                    } else if (coords[1] > 0){
                        nbrs_boat_indicator[DOWN] = true;
                    }
                }
            } else {
                // randomly choose new place to go to
                int dir_1 = bernoulli(0.5);
                int dir_2 = bernoulli(0.5);

                if (!dir_1 && !dir_2) {
                    nbrs_boat_indicator[UP] = true;
                } else if (!dir_1 && dir_2) {
                    nbrs_boat_indicator[RIGHT] = true;
                } else if (dir_1 && !dir_2) {
                    nbrs_boat_indicator[DOWN] = true;
                } else {
                    nbrs_boat_indicator[LEFT] = true;
                }
            }
        } else {
            nbrs_boat_indicator[0] = 0;
            nbrs_boat_indicator[1] = 0;
            nbrs_boat_indicator[2] = 0;
            nbrs_boat_indicator[3] = 0;
        }

        // MPI LOG
        int MAX_TEXT_LENGTH = 80;
        char text[MAX_TEXT_LENGTH];
        int chars_written;
        if (rank == 0) {
            if (boat == 1) {
                chars_written = snprintf(text, MAX_TEXT_LENGTH, "Timestamp %d: I'm process %d at (%d, %d) - Boat at habour with %d capacity.", t, rank, coords[0], coords[1], boat_value);
            } else {
                chars_written = snprintf(text, MAX_TEXT_LENGTH, "Timestamp %d: I'm process %d at (%d, %d) - Boat not at harbour.", t, rank, coords[0], coords[1]);
            }
        } else {
            chars_written = snprintf(text, MAX_TEXT_LENGTH, "Timestamp %d: I'm process %d at (%d, %d) - %u shark(s), %u fish, and %d boat.", t, rank, coords[0], coords[1], shark, fish, boat);
        }
        MPI_Offset disp = (MPI_Offset)rank * (TIME + 1) * MAX_TEXT_LENGTH + MAX_TEXT_LENGTH * t;
        // Ensure that the text buffer is padded with spaces if not fully utilized
        if (chars_written < MAX_TEXT_LENGTH - 1)
        {
            // Fill the remaining space in the buffer with spaces
            memset(text + chars_written, ' ', MAX_TEXT_LENGTH - 1 - chars_written);
        }
        // Add newline character at the end of the line
        text[MAX_TEXT_LENGTH - 1] = '\n';
        MPI_File_write_at(outFile, disp, text, MAX_TEXT_LENGTH, MPI_CHAR, &status);

        // -----------------------------------------------
        // ----------------NON BLOCKING END---------------
        // -----------------------------------------------

        // Gather cell_type values from all processes onto root (rank 0)
        int n_fish_global, n_sharks_global;
        float population_change_fish, population_change_sharks;

        // Perform lotka-volterra every OFFSET step
        // This approach is similar to split-step timestepping
        if ((t % OFFSET) == 0)
        {
            // calculate the total number of fish
            MPI_Reduce(&fish, &n_fish_global, 1, MPI_UNSIGNED, MPI_SUM, 1, MPI_COMM_WORLD);

            // calculate the total number of sharks
            MPI_Reduce(&shark, &n_sharks_global, 1, MPI_UNSIGNED, MPI_SUM, 1, MPI_COMM_WORLD);

            // Put the load not on the root with global_rank = 0, but on the next grid cell with global_rank = 1
            if (rank == 1)
            {
                // calculate the fish and shark density
                tuple init = {(float)n_fish_global / (ROW * COL), (float)n_sharks_global / (ROW * COL)};

                // perform predator-prey simulation and get new population density
                tuple updated = lotka_volterra(init, alpha, beta, gamma, delta, OFFSET, 5 * OFFSET);

                // calculate the population change dynmaics
                population_change_fish = updated.first / init.first;
                population_change_sharks = updated.second / init.second;
            }

            // send updated population dynamcis over all fish
            MPI_Bcast(&population_change_fish, 1, MPI_FLOAT, 1, MPI_COMM_WORLD);
            MPI_Bcast(&population_change_sharks, 1, MPI_FLOAT, 1, MPI_COMM_WORLD);

            float remainder;

            // update the number of fish in each cell (randomised rounding according to remainder)
            remainder = (fish * population_change_fish) - ((unsigned int)(fish * population_change_fish));
            fish = (unsigned int)(fish * population_change_fish) + bernoulli(remainder);

            // update the number of sharks in each cell (randomised rounding according to remainder)
            remainder = (shark * population_change_sharks) - ((unsigned int)(shark * population_change_sharks));
            shark = (unsigned int)(shark * population_change_sharks) + bernoulli(remainder);

            //--------------------------------------------
            // BEGIN DEBUG ONLY
            //--------------------------------------------

            unsigned int fish_map[numtasks], shark_map[numtasks], boat_map[numtasks]; // Array to store gathered cell_type values
            int boat_rank;

            // Gather cell_type values from all processes onto root (rank 0)
            MPI_Gather(&fish, 1, MPI_UNSIGNED, fish_map, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
            MPI_Gather(&shark, 1, MPI_UNSIGNED, shark_map, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
            MPI_Gather(&boat, 1, MPI_UNSIGNED, boat_map, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);

            int coord[2] = {1, 1};

            if (rank == 0)
            {
                // get the total number of fish and sharks
                n_fish_global = n_sharks_global = 0U;
                for (int i = 0; i < ROW * COL; i++)
                {
                    n_fish_global += fish_map[i];
                    n_sharks_global += shark_map[i];
                }
                printf("After this timestep and population dynamics update\n #Fish: %d, #Sharks %d \n", n_fish_global, n_sharks_global);

                unsigned int fish_grid[ROW * COL], shark_grid[ROW * COL];
                for (int i = 0; i < ROW * COL; i++)
                {
                    int c[2];
                    MPI_Cart_coords(comm_2d, i, 2, c);
                    fish_grid[(c[0] * COL + c[1])] = fish_map[i];
                    shark_grid[(c[0] * COL + c[1])] = shark_map[i];
                }
                for (int i = 0; i < ROW; i++)
                {
                    for (int j = 0; j < COL; j++)
                    {
                        printf("(%d, %d)", fish_grid[i * COL + j], shark_grid[i * COL + j]);
                    }
                    printf("\n");
                }
                for (int k = 0; k < numtasks; k++)
                {
                    if (boat_map[k])
                    {
                        boat_rank = k;
                    }
                }
                int boat_rank_coord[2];
                MPI_Cart_coords(comm_2d, boat_rank, 2, boat_rank_coord);
                tuple boat_coord = {boat_rank_coord[0], boat_rank_coord[1]};
                plot_grid(fish_grid, n_fish_global, shark_grid, n_sharks_global, ROW, COL, t, boat_coord);
            }

            //-------------------------------------------------
            // END DEBUG
            //-------------------------------------------------
        }
        
    }
    //***************************************************
    //****************END REPEAT*************************
    //***************************************************

    MPI_File_close(&outFile);

    MPI_Comm_free(&comm_2d);
    MPI_Finalize();
    return 0;
}
