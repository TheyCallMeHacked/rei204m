#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>
#include "perlin.h"

#define ROW 6
#define COL 6
#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3

#define THRESHOLD 0.1f
#define LANDMASS 0
#define WATER 1



int main (int argc, char** argv) {

	for (int i = 0; i < ROW; i++) {
		for (int j = 0; j < COL; j++) {
			float cell_type = perlin(i*0.1f, j*0.1f);// > THRESHOLD;
			printf("%f", cell_type);
		}
		printf("\n");
	}


	return 0;
}

